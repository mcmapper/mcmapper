/* That's pronounced "EmCee" mapper.
 *
 * Build (requires FreeImage):
 *   gcc -o mcmapper main.c -lfreeimage
 *   gcc -DPROTO -o protomapper main.c -lfreeimage
 *
 * Usage:
 *   ./mcmapper <path/to/MCKids.nes>
 *   ./protomapper <path/to/MCKidsPrototype.nes>
 *
 * mcmapper will output images to a directory named after the ROM filename.
 */

/*
    Copyright (c) 2014, Andrew Eikum <coldpies@gmail.com>
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:
        * Redistributions of source code must retain the above copyright
          notice, this list of conditions and the following disclaimer.
        * Redistributions in binary form must reproduce the above copyright
          notice, this list of conditions and the following disclaimer in the
          documentation and/or other materials provided with the distribution.
        * The names of its contributors may not be used to endorse or promote
          products derived from this software without specific prior written
          permission.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL ITS CONTRIBUTORS BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.



    ...that said, if you end up making use of this software, I'd love to know
    about it!
*/

#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <libgen.h>

#include <FreeImage.h>

#ifndef PROTO
static struct {
    size_t offs;
    const char *name;
    int has_kid_spot;
} level_names[] = {
    {0x0df53, "mckids_title"},
    {0x0e010, "mckids_ronald_1", 1}, /* Lvl1: The Garden */
    {0x0e45d, "mckids_ronald_2", 1}, /* Lvl2: Gopher Grove */
    {0x3bca2, "mckids_ronald_2_s"},
    {0x0b279, "mckids_ronald_3", 1}, /* Lvl3: Lazy Leaves */
    {0x0ea0b, "mckids_ronald_4", 1}, /* Lvl4: Mountain View */
    {0x0b709, "mckids_ronald_5", 1}, /* Lvl5: Hidden Glen */
    {0x0c010, "mckids_ronald_6", 1}, /* Lvl6: Towering Trees */
    {0x036a0, "mckids_ronald_complete1"},
    {0x036e8, "mckids_ronald_complete2"},
    {0x0bd04, "mckids_ronald_house"}, /* The Clubhouse */
    {0x124e0, "mckids_ronald_map1"},
    {0x12578, "mckids_ronald_map2"},
    {0x12617, "mckids_ronald_map3"},
    {0x0a610, "mckids_birdy_1", 1}, /* Lvl1: The Stratosphere */
    {0x3ee7f, "mckids_birdy_1_s"},
    {0x0f810, "mckids_birdy_2", 1}, /* Lvl2: Slippery Slopes */
    {0x3ebd1, "mckids_birdy_2_s"},
    {0x10010, "mckids_birdy_3", 1}, /* Lvl3: Icy Maze */
    {0x0aa6c, "mckids_birdy_4", 1}, /* Lvl4: Secret Suprize */
    {0x3c010, "mckids_birdy_5", 1}, /* Lvl5: Frozen Caverns */
    {0x109bc, "mckids_birdy_6", 1}, /* Lvl6: Frosty Mountain */
    {0x035e1, "mckids_birdy_complete"},
    {0x0bd73, "mckids_birdy_house"}, /* The Treehouse */
    {0x3fbed, "mckids_birdy_map1"},
    {0x3fd3d, "mckids_birdy_map2"},
    {0x3fc93, "mckids_birdy_map3"},
    {0x0381a, "mckids_grimace_1", 1}, /* Lvl1: Cliffton Heights */
    {0x3be47, "mckids_grimace_1_s"},
    {0x05690, "mckids_grimace_2", 1}, /* Lvl2: Moose Bridge */
    {0x076e3, "mckids_grimace_3", 1}, /* Lvl3: Lumberjack Lane */
    {0x09510, "mckids_grimace_4", 1}, /* Lvl4: Fry Kid Falls */
    {0x3ddbd, "mckids_grimace_4_s"},
    {0x0d544, "mckids_grimace_5", 1}, /* Lvl5: Danger Lake */
    {0x0d9db, "mckids_grimace_6", 1}, /* Lvl6: Far Falls Forest */
    {0x03797, "mckids_grimace_complete"},
    {0x0bdfc, "mckids_grimace_house"}, /* The Loft */
    {0x120a5, "mckids_grimace_map1"},
    {0x12010, "mckids_grimace_map2"},
    {0x3ff75, "mckids_grimace_map3"},
    {0x0ae8e, "mckids_prof_1", 1}, /* Lvl1: Ports O'Comets */
    {0x3f00f, "mckids_prof_1_s"},
    {0x0c3a0, "mckids_prof_2", 1}, /* Lvl2: Dino Dunes */
    {0x3d87b, "mckids_prof_3", 1}, /* Lvl3: Magic Forest */
    {0x0cbd0, "mckids_prof_4", 1}, /* Lvl4: Desert Cove */
    {0x11023, "mckids_prof_5", 1}, /* Lvl5: Captain's Ship */
    {0x0be76, "mckids_prof_house"}, /* The Workshop */
    {0x12286, "mckids_prof_map1"},
    {0x1231d, "mckids_prof_map2"},
    {0x0ee3b, "mckids_cosmc_1", 1}, /* Lvl1: Moon Monsters */
    {0x3e010, "mckids_cosmc_2", 1}, /* Lvl2: Swisserland */
    {0x0f303, "mckids_cosmc_3", 1}, /* Lvl3: Constellations */
    {0x3f28c, "mckids_cosmc_3_s"},
    {0x0365b, "mckids_cosmc_complete"},
    {0x0bee4, "mckids_cosmc_house"}, /* The Getaway */
    {0x3ff14, "mckids_cosmc_map1"},
    {0x11508, "mckids_hamburglar_1", 1}, /* Lvl1: Burning Bridges */
    {0x3ac10, "mckids_hamburglar_2", 1}, /* Lvl2: Lava Belts */
    {0x3b6a6, "mckids_hamburglar_3", 1}, /* Lvl3: Molten Madness */
    {0x03550, "mckids_hamburglar_complete"},
    {0x0fea7, "mckids_hamburglar_house"}, /* Hamburglar */
    {0x12135, "mckids_hamburglar_map1"},
    {0x121d8, "mckids_hamburglar_map2"},
    {0x3c92d, "mckids_puzzle_1", 1}, /* Lvl1: Phony Finishes */
    {0x3cd98, "mckids_puzzle_2", 1}, /* Lvl2: Clowning Around */
    {0x3d363, "mckids_puzzle_3", 1}, /* Lvl3: zippety Do Da */
    {0x0bf50, "mckids_puzzle_house"}, /* The Secret House */
    {0x3fe74, "mckids_puzzle_map1"},
    {0x09f34, "mckids_bonus_arena", 1},
    {0x05e33, "mckids_bonus_1", 1},
    {0x05ea4, "mckids_bonus_2", 1},
    {0x05efa, "mckids_bonus_3", 1},
    {0x05f60, "mckids_bonus_4", 1},
    {0x03fa9, "mckids_bonus_5", 1},

    /* not in proto */
    {0x03786, "mckids_intro"},
    {0x126b5, "mckids_ronald_map4"},
    {0x12756, "mckids_ronald_map5"},
    {0x123b1, "mckids_prof_map3"},
    {0x12448, "mckids_prof_map4"},
    {0x07cd5, "mckids_cosmc_4"}, /* Lvl4: Spring Fever */
    {0x3f409, "mckids_hamburglar_1_s"},
    {0x11c40, "mckids_hamburglar_4"}, /* Lvl4: Magic Cave */
    {0x3fde7, "mckids_puzzle_map2"},
    {0x07fcc, "mckids_bonus_intro"},
    {0x0ff7a, "mckids_ready_mack1"},
    {0x11fb6, "mckids_ready_mack2"},
    {0x09fb1, "mckids_ready_mick1"},
    {0x11f5f, "mckids_ready_mick2"},
    {0x3df76, "mckids_gameover"},
    {0x3f714, "mckids_victory"},
    {0x3f7bd, "mckids_credits"},
};
#else
static struct {
    size_t offs;
    const char *name;
    int has_kid_spot;
} level_names[] = {
    {0x13f2a, "mckproto_title"},
    
    {0x0c010, "mckproto_ronald_1", 1}, /* Lvl1: Merry Meadow */
    {0x0c4fe, "mckproto_ronald_2", 1}, /* Lvl2: Mushy Meadow */
    {0x11de3, "mckproto_ronald_2_s"},
    {0x08e77, "mckproto_ronald_3", 1}, /* Lvl3: Happy Forest */
    {0x0ca89, "mckproto_ronald_4", 1}, /* Lvl4: Mucho Meadow */
    {0x093f2, "mckproto_ronald_5", 1}, /* Lvl5: Leaky Forest */
    {0x09a1c, "mckproto_ronald_6", 1}, /* Lvl6: M Forest */
    {0x09eab, "mckproto_ronald_complete1"},
    {0x09e23, "mckproto_ronald_complete2"},
    {0x0fd15, "mckproto_ronald_house"}, /* The Clubhouse */
    {0x15d56, "mckproto_ronald_map1"},
    {0x15dfc, "mckproto_ronald_map2"},
    {0x15ea9, "mckproto_ronald_map3"},
    {0x08010, "mckproto_birdy_1", 1}, /* Lvl1: The Stratosphere */
    {0x14e1d, "mckproto_birdy_1_s"},
    {0x0d8ee, "mckproto_birdy_2", 1}, /* Lvl2: Caution, Snow! */
    {0x13a92, "mckproto_birdy_2_s"},
    {0x0e010, "mckproto_birdy_3", 1}, /* Lvl3: Slippery */
    {0x084c6, "mckproto_birdy_4", 1}, /* Lvl4: Puffy Clouds */
    {0x12010, "mckproto_birdy_5", 1}, /* Lvl5: Treacherous Secret */
    {0x0ea75, "mckproto_birdy_6", 1}, /* Lvl6: Snow Clouds */
    {0x0bc54, "mckproto_birdy_complete"},
    {0x07f93, "mckproto_birdy_complete2"},
    {0x0fd94, "mckproto_birdy_house"}, /* The Treehouse */
    {0x155d9, "mckproto_birdy_map1"},
    {0x1573d, "mckproto_birdy_map2"},
    {0x15689, "mckproto_birdy_map3"},
    {0x06010, "mckproto_grimace_1", 1}, /* Lvl1: Cliffyland */
    {0x13d7b, "mckproto_grimace_1_s"},
    {0x067f4, "mckproto_grimace_2", 1}, /* Lvl2: Cliffaroni */
    {0x06e1b, "mckproto_grimace_3", 1}, /* Lvl3: The Cliffs */
    {0x07422, "mckproto_grimace_4", 1}, /* Lvl4: Too High! */
    {0x14cdd, "mckproto_grimace_4_s"},
    {0x0b19a, "mckproto_grimace_5", 1}, /* Lvl5: Boatland */
    {0x0b66c, "mckproto_grimace_6", 1}, /* Lvl6: Lakes in the Sky */
    {0x07ee1, "mckproto_grimace_complete"},
    {0x0fe30, "mckproto_grimace_house"}, /* The Loft */
    {0x15a4e, "mckproto_grimace_map1"},
    {0x159af, "mckproto_grimace_map2"},
    {0x15910, "mckproto_grimace_map3"},
    {0x08982, "mckproto_prof_1", 1}, /* Lvl1: Dock on the Water */
    {0x14fe2, "mckproto_prof_1_s"},
    {0x0a010, "mckproto_prof_2", 1}, /* Lvl2: Vacation Island */
    {0x134aa, "mckproto_prof_3", 1}, /* Lvl3: Magic Forest */
    {0x0a823, "mckproto_prof_4", 1}, /* Lvl4: Dinosaur Island */
    {0x0f628, "mckproto_prof_5", 1}, /* Lvl5: The Raging Queen */
    {0x0febb, "mckproto_prof_house"}, /* The Workshop */
    {0x15c10, "mckproto_prof_map1"},
    {0x15cb3, "mckproto_prof_map2"},
    {0x0cf4e, "mckproto_cosmc_1", 1}, /* Lvl1: Low Gravity */
    {0x14010, "mckproto_cosmc_2", 1}, /* Lvl2: Say Cheeeezzz */
    {0x0d415, "mckproto_cosmc_3", 1}, /* Lvl3: The Sky's the Limit */
    {0x15295, "mckproto_cosmc_3_s"},
    {0x15437, "mckproto_cosmc_3_s_dup"},
                                   /* Lvl4: Where am I */
    {0x09f09, "mckproto_cosmc_complete"},
    {0x0ff37, "mckproto_cosmc_house"}, /* The Getaway */
    {0x1589c, "mckproto_cosmc_map1"},
    {0x10010, "mckproto_hamburglar_1", 1}, /* Lvl1: Hot Hot Hot */
    {0x10715, "mckproto_hamburglar_2", 1}, /* Lvl2: Hot Lava Machines */
    {0x1119d, "mckproto_hamburglar_3", 1}, /* Lvl3: Hot foot */
                                        /* Lvl4: The Magic Bag */
    {0x0bce9, "mckproto_hamburglar_complete"},
    {0x11c82, "mckproto_hamburglar_house"}, /* Hamburglar */
    {0x15aec, "mckproto_hamburglar_map1"},
    {0x15b75, "mckproto_hamburglar_map2"},
    {0x117b1, "mckproto_puzzle_1", 1}, /* Lvl1: Place those blocks */
    {0x1292e, "mckproto_puzzle_2", 1}, /* Lvl2: Ronald's Help */
    {0x12f48, "mckproto_puzzle_3", 1}, /* Lvl3: Zipperland */
    {0x11d64, "mckproto_puzzle_house"}, /* The Secret House */
    {0x157f2, "mckproto_puzzle_map1"},
    {0x0fc88, "mckproto_bonus_arena", 1},
    {0x0bf4c, "mckproto_bonus_1", 1},
    {0x0fb58, "mckproto_bonus_2", 1},
    {0x0fbae, "mckproto_bonus_3", 1},
    {0x0fc17, "mckproto_bonus_4", 1},
    {0x09f7f, "mckproto_bonus_5", 1},

    /* not in final */
    {0x0bdb4, "mckproto_unused_1", 1},
    {0x0f134, "mckproto_unused_2", 1},
};
#endif

static const char *rom_file;
static int fd;
static unsigned int valid = 0;
static uint32_t bits = 0;

static struct level {
    unsigned char start_x;
    unsigned char start_y;
    unsigned char bg; /* universal background color */
    unsigned char flags; /* flags? affects scrolling, some graphics */
    unsigned char enemy_group; /* enemy group */
    unsigned char enemy_palette1; /* enemy palette */
    unsigned char enemy_palette2; /* 2nd enemy palette */
    unsigned char junk6; /* ??? */
    unsigned char card1;
    unsigned char card2;
#ifndef PROTO
    unsigned char music;
#endif
    unsigned char addr_low;
    unsigned char addr_high;
    unsigned char addr_bank;
    unsigned char width;
    unsigned char height;
    unsigned char ts1, ts2, ts3, ts4;
    unsigned char data[0x2000];
    struct {
        unsigned char *x;
        unsigned char *y;
        unsigned char *type;
        unsigned char *order;
    }actors;
    unsigned char num_actors;
    unsigned char *map;
} levels[0xFF];

static struct bank {
    unsigned int offs;
    unsigned int seek;
} banks[] = {
    {0},
    {0x2000u * 1, 0xa000}, /*0x01*/
    {0x2000u * 2, 0xa000}, /*0x02*/
    {0x2000u * 3, 0xa000}, /*0x03*/
    {0x2000u * 4, 0xa000}, /*0x04*/
    {0x2000u * 5, 0xa000}, /*0x05*/
    {0x2000u * 6, 0xa000}, /*0x06*/
    {0x2000u * 7, 0xa000}, /*0x07*/
    {0x2000u * 8, 0xa000}, /*0x08*/
    {0x2000u * 9, 0xa000}, /*0x09*/
    {0x2000u * 0xa, 0xa000}, /*0x0a*/
    {0x2000u * 0xb, 0xa000}, /*0x0b*/
    {0x2000u * 0xc, 0xa000}, /*0x0c*/
    {0x3a000u, 0}, /*0x0d*/
    {0x3c000u, 0}, /*0x0e*/
    {0x3e000u, 0} /*0x0f*/
};

/* Adapted from TCRF's NTSC Palette:
 * http://tcrf.net/Help:Contents/Taking_Screenshots#NES
 * Listed here in BGR */
static RGBQUAD colors[0x40] = {
    {0x66, 0x66, 0x66},
    {0x88, 0x2a, 0x00},
    {0xa7, 0x12, 0x14},
    {0xa4, 0x00, 0x3b},
    {0x7e, 0x00, 0x5c},
    {0x40, 0x00, 0x6e},
    {0x00, 0x07, 0x6c},
    {0x00, 0x1d, 0x56},
    {0x00, 0x35, 0x33},
    {0x00, 0x48, 0x0c},
    {0x00, 0x52, 0x00},
    {0x08, 0x4f, 0x00},
    {0x4d, 0x40, 0x00},
    {0x00, 0x00, 0x00},
    {0x00, 0x00, 0x00},
    {0x00, 0x00, 0x00},
    {0xad, 0xad, 0xad},
    {0xd9, 0x5f, 0x15},
    {0xff, 0x40, 0x42},
    {0xfe, 0x27, 0x75},
    {0xcc, 0x1a, 0xa0},
    {0x7b, 0x1e, 0xb7},
    {0x20, 0x31, 0xb5},
    {0x00, 0x4e, 0x99},
    {0x00, 0x6d, 0x6b},
    {0x00, 0x87, 0x38},
    {0x00, 0x93, 0x0d},
    {0x32, 0x8f, 0x00},
    {0x8d, 0x7c, 0x00},
    {0x00, 0x00, 0x00},
    {0x00, 0x00, 0x00},
    {0x00, 0x00, 0x00},
    {0xff, 0xff, 0xff},
    {0xff, 0xb0, 0x64},
    {0xff, 0x90, 0x92},
    {0xff, 0x76, 0xc6},
    {0xff, 0x6a, 0xf2},
    {0xcc, 0x6e, 0xff},
    {0x70, 0x81, 0xff},
    {0x22, 0x9e, 0xea},
    {0x00, 0xbe, 0xbc},
    {0x00, 0xd8, 0x88},
    {0x30, 0xe4, 0x5c},
    {0x82, 0xe0, 0x45},
    {0xde, 0xcd, 0x48},
    {0x4f, 0x4f, 0x4f},
    {0x00, 0x00, 0x00},
    {0x00, 0x00, 0x00},
    {0xff, 0xff, 0xff},
    {0xff, 0xdf, 0xc0},
    {0xff, 0xd2, 0xd3},
    {0xff, 0xc8, 0xe8},
    {0xff, 0xc2, 0xfa},
    {0xea, 0xc4, 0xff},
    {0xc5, 0xcc, 0xff},
    {0xa5, 0xd8, 0xf7},
    {0x94, 0xe5, 0xe4},
    {0x96, 0xef, 0xcf},
    {0xab, 0xf4, 0xbd},
    {0xcc, 0xf3, 0xb3},
    {0xf2, 0xeb, 0xb5},
    {0xb8, 0xb8, 0xb8},
    {0x00, 0x00, 0x00},
    {0x00, 0x00, 0x00}
};

static struct {
    unsigned char c1[0x20];
    unsigned char c2[0x20];
    unsigned char c3[0x20];
} palettes;

static struct {
#ifndef PROTO
    unsigned char c1[0xf];
    unsigned char c2[0xf];
    unsigned char c3[0xf];
#else
    unsigned char c1[0xa];
    unsigned char c2[0xa];
    unsigned char c3[0xa];
#endif
} alt_palettes;

static struct tileset {
    unsigned char gfx_bank;
    unsigned char palettes[4];
    unsigned char tile_data[512];
    unsigned char *tile_table;
    unsigned char width1, width2;
    unsigned char chr_raw[0x400];
} tilesets[0x2c];

static struct enemy_group {
    unsigned char chr_raw[0x800];
} enemy_groups[0x14];

static unsigned char std_actor[0x800];

static RGBQUAD *bg;


/*
:'a,'bs/\./0x40, /g
:'a,'bs/s/0x14, /g

 */

/*
ssssssss........
ssssssss........
ss..............
ss..............
ss..............
ss....ssssssss..
ss...sssssssss..
....ss..........
....ss..........
....sssssssss...
.....sssssssss..
............ss..
............ss..
....sssssssss...
....ssssssss....
................
 */
static const unsigned char IMG_START[] = {
0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 
0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 
0x14, 0x14, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 
0x14, 0x14, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 
0x14, 0x14, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 
0x14, 0x14, 0x40, 0x40, 0x40, 0x40, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x40, 0x40, 
0x14, 0x14, 0x40, 0x40, 0x40, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x40, 0x40, 
0x40, 0x40, 0x40, 0x40, 0x14, 0x14, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 
0x40, 0x40, 0x40, 0x40, 0x14, 0x14, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 
0x40, 0x40, 0x40, 0x40, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x40, 0x40, 0x40, 
0x40, 0x40, 0x40, 0x40, 0x40, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x40, 0x40, 
0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x14, 0x14, 0x40, 0x40, 
0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x14, 0x14, 0x40, 0x40, 
0x40, 0x40, 0x40, 0x40, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x40, 0x40, 0x40, 
0x40, 0x40, 0x40, 0x40, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x40, 0x40, 0x40, 0x40, 
0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 
};

/*
................
................
................
................
................
......ssss......
....ssssssss....
....ss....ss....
....ss....ss....
....ss....ss....
....ssssssss....
....ssssssss....
....ss....ss....
....ss....ss....
....ss....ss....
................
 */
static const unsigned char IMG_ACTOR[] = {
0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 
0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 
0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 
0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 
0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 
0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x14, 0x14, 0x14, 0x14, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 
0x40, 0x40, 0x40, 0x40, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x40, 0x40, 0x40, 0x40, 
0x40, 0x40, 0x40, 0x40, 0x14, 0x14, 0x40, 0x40, 0x40, 0x40, 0x14, 0x14, 0x40, 0x40, 0x40, 0x40, 
0x40, 0x40, 0x40, 0x40, 0x14, 0x14, 0x40, 0x40, 0x40, 0x40, 0x14, 0x14, 0x40, 0x40, 0x40, 0x40, 
0x40, 0x40, 0x40, 0x40, 0x14, 0x14, 0x40, 0x40, 0x40, 0x40, 0x14, 0x14, 0x40, 0x40, 0x40, 0x40, 
0x40, 0x40, 0x40, 0x40, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x40, 0x40, 0x40, 0x40, 
0x40, 0x40, 0x40, 0x40, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x14, 0x40, 0x40, 0x40, 0x40, 
0x40, 0x40, 0x40, 0x40, 0x14, 0x14, 0x40, 0x40, 0x40, 0x40, 0x14, 0x14, 0x40, 0x40, 0x40, 0x40, 
0x40, 0x40, 0x40, 0x40, 0x14, 0x14, 0x40, 0x40, 0x40, 0x40, 0x14, 0x14, 0x40, 0x40, 0x40, 0x40, 
0x40, 0x40, 0x40, 0x40, 0x14, 0x14, 0x40, 0x40, 0x40, 0x40, 0x14, 0x14, 0x40, 0x40, 0x40, 0x40, 
0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 0x40, 
};

static void write_overlay(FIBITMAP *img, unsigned int ix, unsigned int iy, const unsigned char *overlay)
{
    unsigned char x, y;
    for(y = 0; y < 16; ++y){
        for(x = 0; x < 16; ++x){
            unsigned char color = overlay[y * 16 + x];
            if(color >= 0x40)
                continue;
            RGBQUAD *quad = &colors[color];
            FreeImage_SetPixelColor(img, ix * 16 + x, iy * 16 + y, quad);
        }
    }
}

#define CHR_MIRROR 1
#define CHR_ALTPALETTE 2

static void write_chr(FIBITMAP *img, unsigned char *chr_raw, unsigned char tile,
        unsigned int ix, unsigned int iy, unsigned char palette, int flags)
{
    unsigned char x, y;
    unsigned char *raw = &chr_raw[tile * 16];
    for(y = 0; y < 8; ++y){
        for(x = 0; x < 8; ++x){
            unsigned char color = ((raw[y] >> (7 - x)) & 1) | (((raw[y + 8] >> (7 - x)) & 1) << 1);
            RGBQUAD *quad;
            switch(color){
                case 0:
                    continue;
                case 1:
                    if(flags & CHR_ALTPALETTE)
                        quad = &colors[alt_palettes.c1[palette]];
                    else
                        quad = &colors[palettes.c1[palette]];
                    break;
                case 2:
                    if(flags & CHR_ALTPALETTE)
                        quad = &colors[alt_palettes.c2[palette]];
                    else
                        quad = &colors[palettes.c2[palette]];
                    break;
                case 3:
                    if(flags & CHR_ALTPALETTE)
                        quad = &colors[alt_palettes.c3[palette]];
                    else
                        quad = &colors[palettes.c3[palette]];
                    break;
            }
            FreeImage_SetPixelColor(img, ix + (flags & CHR_MIRROR ? 7 - x : x), iy + y, quad);
        }
    }
}

static void write_tile_image(FIBITMAP *img, struct level *level, size_t x, size_t y)
{
    unsigned char tset = (level->map[y * level->width + x] >> 6) & 0x3;
    unsigned char tile = level->map[y * level->width + x] & 0x3F;
    unsigned char ts;
    unsigned char palette;
    struct tileset *set;

    switch(tset){
    case 0:
        ts = level->ts1;
        break;
    case 1:
        ts = level->ts2;
        break;
    case 2:
        ts = level->ts3;
        break;
    case 3:
        ts = level->ts4;
        break;
    }

    set = &tilesets[ts];

    palette = set->tile_table[tile + set->width1 * 4];
    if(level->flags & 1) /* ??? */
        palette += 8;

    /*
    printf("tile[0x%02x.0x%02x]: 0x%02x 0x%02x 0x%02x 0x%02x [0x%02x]\n", 
            (unsigned int)ts, (unsigned int)tile,
            (unsigned int)set->tile_table[tile + set->width1 * 0],
            (unsigned int)set->tile_table[tile + set->width1 * 1],
            (unsigned int)set->tile_table[tile + set->width1 * 2],
            (unsigned int)set->tile_table[tile + set->width1 * 3],
            palette);
            */

    write_chr(img, set->chr_raw, set->tile_table[tile + set->width1 * 0], x * 16 + 0, y * 16 + 0, palette, 0);
    write_chr(img, set->chr_raw, set->tile_table[tile + set->width1 * 1], x * 16 + 8, y * 16 + 0, palette, 0);
    write_chr(img, set->chr_raw, set->tile_table[tile + set->width1 * 2], x * 16 + 0, y * 16 + 8, palette, 0);
    write_chr(img, set->chr_raw, set->tile_table[tile + set->width1 * 3], x * 16 + 8, y * 16 + 8, palette, 0);
}

/* so, i got the palettes totally wrong. apparently the actors are smart enough
 * to choose the correct palette, so long the correct palette is loaded into
 * either of the enemy_palette positions. what I have here works, but really
 * they should all just be hard-coded. but i'm lazy, so I just hard-coded the
 * broken ones */
static void write_actor(FIBITMAP *img, struct level *level, int idx)
{
    unsigned int x, y;
    unsigned char *chr_raw, p;

    static const unsigned char std_palette = 0x00;

    x = level->actors.x[idx] * 16;
    y = level->actors.y[idx] * 16 + 8;

    switch(level->actors.type[idx]){
#ifndef PROTO
    case 0x01: /* victory arch */
        chr_raw = std_actor;
        write_chr(img, chr_raw, 0x6d, x + 0x00, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x6e, x + 0x08, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7d, x + 0x00, y - 0x00, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7e, x + 0x08, y - 0x00, std_palette, CHR_ALTPALETTE);
        break;
    case 0x19: /* title credits seq */
		break;
    case 0x20: /* bear */
    case 0x21: /* bear (dumb) */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = 0x02;
        write_chr(img, chr_raw, 0x50, x + 0x08, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x51, x + 0x00, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x60, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x61, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x70, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x71, x + 0x00, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
    case 0x22: /* hopping chick */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette1;
        write_chr(img, chr_raw, 0x36, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x37, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x46, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x47, x + 0x00, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
    /* 0x23, 0x24: nil? */
    case 0x25: /* vulture */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = std_palette;
        write_chr(img, chr_raw, 0x05, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x06, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x15, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x16, x + 0x00, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x25, x + 0x08, y + 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x26, x + 0x00, y + 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
    /* 0x26: XXX some floating behavior?? maybe stationary vulture? */
    case 0x27: /* http://www.homestarrunner.com/sbemail145.html */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette1;
        write_chr(img, chr_raw, 0x40, x + 0x08, y - 0x18, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x41, x + 0x00, y - 0x18, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x50, x + 0x08, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x51, x + 0x00, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x60, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x61, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x70, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x71, x + 0x00, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
    case 0x28: /* high jumping fish */
    case 0x29: /* jumping fish */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = 0x0e;
        write_chr(img, chr_raw, 0x66, x + 0x00, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x67, x + 0x08, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x76, x + 0x00, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x77, x + 0x08, y - 0x00, p, CHR_ALTPALETTE);
        break;
    case 0x2a: /* falling comets */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette2;
        write_chr(img, chr_raw, 0x00, x - 0x08, y - 0x18, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x01, x + 0x00, y - 0x18, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x02, x + 0x08, y - 0x18, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x10, x - 0x08, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x11, x + 0x00, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x12, x + 0x08, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x21, x + 0x00, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x22, x + 0x08, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0f, x + 0x10, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1e, x + 0x08, y + 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1f, x + 0x10, y + 0x00, p, CHR_ALTPALETTE);
        break;
    case 0x2b: /* spider */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = std_palette;
        write_chr(img, chr_raw, 0x00, x + 0x0c, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x10, x + 0x08, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x11, x + 0x10, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x20, x + 0x08, y + 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x21, x + 0x10, y + 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x30, x + 0x08, y + 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x31, x + 0x10, y + 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x40, x + 0x08, y + 0x18, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x41, x + 0x10, y + 0x18, p, CHR_ALTPALETTE);
        break;
    case 0x2c: /* moose */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette1;
        write_chr(img, chr_raw, 0x01, x + 0x10, y - 0x20, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x02, x + 0x08, y - 0x20, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x03, x + 0x00, y - 0x20, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x12, x + 0x08, y - 0x18, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x13, x + 0x00, y - 0x18, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x22, x + 0x08, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x23, x + 0x00, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x30, x + 0x10, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x31, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x40, x + 0x10, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x41, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
    case 0x2d: /* hopping flower */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = std_palette;
        write_chr(img, chr_raw, 0x5e, x + 0x00, y - 0x18, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x5f, x + 0x08, y - 0x18, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0b, x + 0x00, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0c, x + 0x08, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1b, x + 0x00, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1c, x + 0x08, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x2b, x + 0x00, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x2c, x + 0x08, y - 0x00, p, CHR_ALTPALETTE);
        break;
    case 0x2e: /* leaf platform */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette2;
        write_chr(img, chr_raw, 0x0d, x + 0x00, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0e, x + 0x08, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0f, x + 0x10, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1d, x + 0x00, y + 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1e, x + 0x08, y + 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1f, x + 0x10, y + 0x00, p, CHR_ALTPALETTE);
        break;
    case 0x2f: /* leaf platform generator */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette2;
        write_chr(img, chr_raw, 0x0d, x + 0x08, y - 0x0c, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0e, x + 0x10, y - 0x0c, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0f, x + 0x18, y - 0x0c, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1d, x + 0x08, y - 0x04, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1e, x + 0x10, y - 0x04, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1f, x + 0x18, y - 0x04, p, CHR_ALTPALETTE);

        write_chr(img, chr_raw, 0x0d, x + 0x00, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0e, x + 0x08, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0f, x + 0x10, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1d, x + 0x00, y + 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1e, x + 0x08, y + 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1f, x + 0x10, y + 0x00, p, CHR_ALTPALETTE);

        write_chr(img, chr_raw, 0x0d, x - 0x08, y - 0x04, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0e, x + 0x00, y - 0x04, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0f, x + 0x08, y - 0x04, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1d, x - 0x08, y + 0x04, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1e, x + 0x00, y + 0x04, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1f, x + 0x08, y + 0x04, p, CHR_ALTPALETTE);
        break;
    /* 0x30: ronald house obj
     * 0x31: birdy house obj
     * 0x32: grimace house obj
     * 0x33: hamburglar house obj
     * 0x34: professor house obj
     * 0x35: cosmc house obj */
    /* 0x36: UNUSED! beehive */
    case 0x37: /* beaver */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette1;
        write_chr(img, chr_raw, 0x69, x + 0x10, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x6a, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x6b, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x79, x + 0x10, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7a, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7b, x + 0x00, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
    /* 0x38: XXX far bottom-left tile, stuck in air, no behavior. */
    /* 0x39: similar. wider object */
    case 0x3a: /* frozen bear */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette1;
        write_chr(img, chr_raw, 0x01, x + 0x00, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x02, x + 0x08, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x11, x + 0x00, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x12, x + 0x08, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x21, x + 0x00, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x22, x + 0x08, y - 0x00, p, CHR_ALTPALETTE);
        break;
    case 0x3b: /* dancing snowman */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette2;
        write_chr(img, chr_raw, 0x0d, x + 0x08, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0e, x + 0x00, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x56, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x57, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x66, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x67, x + 0x00, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
    case 0x3c: /* abominable snowman */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette2;
        write_chr(img, chr_raw, 0x58, x + 0x08, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x59, x + 0x00, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x68, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x69, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x78, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x79, x + 0x00, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
    /* 0x3d: XXX something glitchy, stationary, and in front of player */
    /* 0x3e-f: nothing. */
    /* 0x40: snowball? */
    /* 0x41: snowball breaking apart? */
    /* 0x42: ash pile ball from boss fight */
    case 0x43: /* angry fish */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette2;
        write_chr(img, chr_raw, 0x2e, x + 0x00, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x2f, x + 0x08, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x3e, x + 0x00, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x3f, x + 0x08, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x4e, x + 0x00, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x4f, x + 0x08, y - 0x00, p, CHR_ALTPALETTE);
        break;
    case 0x44: /* hermit crab */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette1;
        write_chr(img, chr_raw, 0x00, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x01, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x10, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x11, x + 0x00, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
    /* 0x45: nil */
    case 0x46: /* drill enemy */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = 0x02;
        write_chr(img, chr_raw, 0x04, x + 0x04, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x08, x + 0x00, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x09, x + 0x08, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x18, x + 0x00, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x19, x + 0x08, y - 0x00, p, CHR_ALTPALETTE);
        break;
    /* 0x47: XXX huge circle thing??? */
    case 0x48: /* helicopter enemy */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette1;
        write_chr(img, chr_raw, 0x0e, x + 0x08, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0f, x + 0x00, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1e, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1f, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x2e, x + 0x08, y + 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x2f, x + 0x00, y + 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
    case 0x49: /* snail */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette1;
        write_chr(img, chr_raw, 0x30, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x31, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x40, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x41, x + 0x00, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
    case 0x4a: /* left facing ghost shooter thing */
    case 0x4b: /* right facing ghost shooter thing */
        /* don't draw, launcher is built into background */
        break;
    case 0x4c: /* little rock dude */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = std_palette;
        write_chr(img, chr_raw, 0x08, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x09, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x18, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x19, x + 0x00, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
    /* 0x4d: credits objects
     * 0x4e: credits objects */
    case 0x4f: /* spring enemy */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette2;
        write_chr(img, chr_raw, 0x0d, x + 0x08, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0c, x + 0x00, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1d, x + 0x08, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1c, x + 0x00, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x2d, x + 0x08, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x2c, x + 0x00, y - 0x00, p, CHR_ALTPALETTE);
        break;
    /* 0x50-2: zipper, see below */
    /* 0x53: nothing */
    case 0x54: /* floating spring */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette2;
        write_chr(img, chr_raw, 0x24, x + 0x00, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x25, x + 0x08, y - 0x00, p, CHR_ALTPALETTE);
        break;
    /* 0x55: nothing */
    /* 0x56: some control object, dunno */
    /* 0x57: intro cutscene wing thing */
    /* 0x58: Not sure, maybe little hat dude from the boss? */
    case 0x59: /* magic bag boss */
        /* the level actually loads a new enemy group halfway
         * through the stage, so just hardcode it here */
        chr_raw = enemy_groups[0x12].chr_raw;
        p = 0x03;
        write_chr(img, chr_raw, 0x5d, x + 0x00, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x5e, x + 0x08, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x5f, x + 0x10, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x6d, x + 0x00, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x6e, x + 0x08, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x6f, x + 0x10, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7d, x + 0x00, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7e, x + 0x08, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7f, x + 0x10, y - 0x00, p, CHR_ALTPALETTE);
        break;
    case 0x5a: /* moon tongue */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette1;
        write_chr(img, chr_raw, 0x3c, x + 0x04, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x4c, x + 0x04, y - 0x00, p, CHR_ALTPALETTE);
        break;
    /* 0x5b: background twinkler? */
    case 0x5c: /* little rock dude on moon */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette1;
        write_chr(img, chr_raw, 0x00, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x01, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x10, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x11, x + 0x00, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
    case 0x5d: /* caveman */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette1;
        write_chr(img, chr_raw, 0x08, x + 0x08, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x09, x + 0x00, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x18, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x19, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x28, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x29, x + 0x00, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
    /* 0x5e: nothing */
    /* 0x5f: some background block? stand on it, otherwise no interaction */
    case 0x50: /* zipper (new level) */
    case 0x51: /* zipper (within level) */
    case 0x52: /* all below are zippers from puzzleworld 3 */
    case 0x80:
    case 0x81:
    case 0x82:
    case 0x83:
    case 0x84:
    case 0x85:
    case 0x86:
    case 0x87:
    case 0x88:
    case 0x89:
    case 0x8a:
    case 0x8b:
    case 0x8c:
    case 0x8d:
    case 0x8e:
    case 0x8f:
    case 0x90:
    case 0x91:
    case 0x92:
    case 0x93:
    case 0x94:
    case 0x95:
    case 0x96:
    case 0x97:
        chr_raw = std_actor;
        write_chr(img, chr_raw, 0x37, x + 0x04, y - 0x20, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x47, x + 0x04, y - 0x18, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x57, x + 0x04, y - 0x10, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x67, x + 0x04, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x77, x + 0x04, y + 0x00, std_palette, CHR_ALTPALETTE);
        break;
    case 0x60: /* pickup block */
        chr_raw = std_actor;
        write_chr(img, chr_raw, 0x4c, x + 0x00, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x4d, x + 0x08, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x5c, x + 0x00, y - 0x00, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x5d, x + 0x08, y - 0x00, std_palette, CHR_ALTPALETTE);
        break;
    case 0x61: /* pickup block slot */
        chr_raw = std_actor;
        write_chr(img, chr_raw, 0x4e, x + 0x00, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x4f, x + 0x08, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x5e, x + 0x00, y - 0x00, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x5f, x + 0x08, y - 0x00, std_palette, CHR_ALTPALETTE);
        break;
    /* 0x6c: puzzleworld map platform
     * 0x6d: professor map obj */
    case 0x70: /* cloud platform (slow) */
    case 0x71: /* cloud platform (med) */
    case 0x72: /* cloud platform (fast) */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = 0x03;
        write_chr(img, chr_raw, 0x6d, x - 0x04, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x6e, x + 0x04, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x6f, x + 0x0c, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7d, x - 0x04, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7e, x + 0x04, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7f, x + 0x0c, y - 0x00, p, CHR_ALTPALETTE);
        break;
    case 0x73: /* conveyor belt platform */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette2;
        write_chr(img, chr_raw, 0x0c, x + 0x04, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0d, x + 0x0c, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0e, x + 0x14, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1c, x + 0x04, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1d, x + 0x0c, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1e, x + 0x14, y - 0x00, p, CHR_ALTPALETTE);
        break;
    case 0x74: /* boat */
        chr_raw = std_actor;
        write_chr(img, chr_raw, 0x28, x + 0x00, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x29, x + 0x08, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x38, x + 0x00, y - 0x00, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x39, x + 0x08, y - 0x00, std_palette, CHR_ALTPALETTE);
        break;
    case 0x76: /* floating platform */
    case 0x77: /* falling platform */
        chr_raw = std_actor;
        write_chr(img, chr_raw, 0x68, x - 0x04, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x69, x + 0x04, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x6a, x + 0x0c, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x78, x - 0x04, y - 0x00, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x79, x + 0x04, y - 0x00, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7a, x + 0x0c, y - 0x00, std_palette, CHR_ALTPALETTE);
        break;
    case 0x78: /* ronald platform */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette2;
        write_chr(img, chr_raw, 0x49, x + 0x00, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x4a, x + 0x08, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x59, x + 0x00, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x5a, x + 0x08, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x69, x + 0x00, y + 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x6a, x + 0x08, y + 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x79, x + 0x00, y + 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7a, x + 0x08, y + 0x10, p, CHR_ALTPALETTE);
        break;
    case 0x79: /* super spring */
    case 0x7a: /* super spring (upside down) */
        chr_raw = std_actor;
        write_chr(img, chr_raw, 0x68, x + 0x00, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x69, x + 0x08, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x69, x + 0x10, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x6a, x + 0x18, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x78, x + 0x00, y - 0x00, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x79, x + 0x08, y - 0x00, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x79, x + 0x10, y - 0x00, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7a, x + 0x18, y - 0x00, std_palette, CHR_ALTPALETTE);
        break;
     /* 0x7b: grimace map obj */
#else
    case 0x01: /* victory arch */
        chr_raw = std_actor;
        write_chr(img, chr_raw, 0x6d, x + 0x00, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x6e, x + 0x08, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7d, x + 0x00, y - 0x00, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7e, x + 0x08, y - 0x00, std_palette, CHR_ALTPALETTE);
        break;
    case 0x20: /* bear */
    case 0x21: /* bear (dumb) */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = /*level->enemy_palette1*/0x02;
        if(level->enemy_group == 0x09){
            /* HACK: fix wrong enemy group bug */
            write_chr(img, chr_raw, 0x44, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
            write_chr(img, chr_raw, 0x45, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
            write_chr(img, chr_raw, 0x54, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
            write_chr(img, chr_raw, 0x55, x + 0x00, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        }else{
            write_chr(img, chr_raw, 0x50, x + 0x08, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
            write_chr(img, chr_raw, 0x51, x + 0x00, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
            write_chr(img, chr_raw, 0x60, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
            write_chr(img, chr_raw, 0x61, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
            write_chr(img, chr_raw, 0x70, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
            write_chr(img, chr_raw, 0x71, x + 0x00, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        }
        break;
    case 0x22: /* hopping chick */
        if(level->enemy_group == 0x09 ||
                level->enemy_group == 0x03)
            chr_raw = enemy_groups[0x0b].chr_raw;
        else
            chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette1;
        write_chr(img, chr_raw, 0x36, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x37, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x46, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x47, x + 0x00, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
    case 0x25: /* vulture */
        if(level->enemy_group == 0x09 ||
                level->enemy_group == 0x03)
            chr_raw = enemy_groups[0x0b].chr_raw;
        else
            chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = std_palette;
        write_chr(img, chr_raw, 0x05, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x06, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x15, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x16, x + 0x00, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x25, x + 0x08, y + 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x26, x + 0x00, y + 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
    case 0x27: /* http://www.homestarrunner.com/sbemail145.html */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette1;
        write_chr(img, chr_raw, 0x0c, x + 0x08, y - 0x18, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0d, x + 0x00, y - 0x18, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1c, x + 0x08, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1d, x + 0x00, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x2c, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x2d, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x3c, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x3d, x + 0x00, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
    case 0x28: /* high jumping fish */
    case 0x29: /* jumping fish */
        if(level->enemy_group == 0x0f)
            chr_raw = enemy_groups[0x0e].chr_raw;
        else
            chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = 0x06;
        write_chr(img, chr_raw, 0x66, x + 0x00, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x67, x + 0x08, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x76, x + 0x00, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x77, x + 0x08, y - 0x00, p, CHR_ALTPALETTE);
        break;
    case 0x2a: /* falling comets */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette2;
        write_chr(img, chr_raw, 0x00, x - 0x08, y - 0x18, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x01, x + 0x00, y - 0x18, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x02, x + 0x08, y - 0x18, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x10, x - 0x08, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x11, x + 0x00, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x12, x + 0x08, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x21, x + 0x00, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x22, x + 0x08, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0f, x + 0x10, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1e, x + 0x08, y + 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1f, x + 0x10, y + 0x00, p, CHR_ALTPALETTE);
        break;
    case 0x2b: /* spider */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = std_palette;
        write_chr(img, chr_raw, 0x00, x + 0x0c, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x10, x + 0x08, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x11, x + 0x10, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x20, x + 0x08, y + 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x21, x + 0x10, y + 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x30, x + 0x08, y + 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x31, x + 0x10, y + 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x40, x + 0x08, y + 0x18, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x41, x + 0x10, y + 0x18, p, CHR_ALTPALETTE);
        break;
    case 0x2c: /* moose */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette1;
        write_chr(img, chr_raw, 0x0d, x + 0x10, y - 0x20, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0e, x + 0x08, y - 0x20, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0f, x + 0x00, y - 0x20, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1e, x + 0x08, y - 0x18, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1f, x + 0x00, y - 0x18, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x2e, x + 0x08, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x2f, x + 0x00, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x3e, x + 0x10, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x3f, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x4e, x + 0x10, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x4f, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
    case 0x2d: /* hopping flower */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = std_palette;
        write_chr(img, chr_raw, 0x5e, x + 0x00, y - 0x18, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x5f, x + 0x08, y - 0x18, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0b, x + 0x00, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0c, x + 0x08, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1b, x + 0x00, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1c, x + 0x08, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x2b, x + 0x00, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x2c, x + 0x08, y - 0x00, p, CHR_ALTPALETTE);
        break;
    case 0x2e: /* leaf platform */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette2;
        write_chr(img, chr_raw, 0x0d, x + 0x00, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0e, x + 0x08, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0f, x + 0x10, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1d, x + 0x00, y + 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1e, x + 0x08, y + 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1f, x + 0x10, y + 0x00, p, CHR_ALTPALETTE);
        break;
    case 0x2f: /* leaf platform generator */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette2;
        write_chr(img, chr_raw, 0x0d, x + 0x08, y - 0x0c, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0e, x + 0x10, y - 0x0c, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0f, x + 0x18, y - 0x0c, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1d, x + 0x08, y - 0x04, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1e, x + 0x10, y - 0x04, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1f, x + 0x18, y - 0x04, p, CHR_ALTPALETTE);

        write_chr(img, chr_raw, 0x0d, x + 0x00, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0e, x + 0x08, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0f, x + 0x10, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1d, x + 0x00, y + 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1e, x + 0x08, y + 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1f, x + 0x10, y + 0x00, p, CHR_ALTPALETTE);

        write_chr(img, chr_raw, 0x0d, x - 0x08, y - 0x04, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0e, x + 0x00, y - 0x04, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0f, x + 0x08, y - 0x04, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1d, x - 0x08, y + 0x04, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1e, x + 0x00, y + 0x04, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1f, x + 0x08, y + 0x04, p, CHR_ALTPALETTE);
        break;
    case 0x36: /* bee hive (not used in final!) */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette1;
        write_chr(img, chr_raw, 0x6c, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x6d, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7c, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7d, x + 0x00, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
    case 0x37: /* beaver */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette1;
        write_chr(img, chr_raw, 0x69, x + 0x10, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x6a, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x6b, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x79, x + 0x10, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7a, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7b, x + 0x00, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
    case 0x3a: /* frozen bear */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette1;
        write_chr(img, chr_raw, 0x50, x + 0x00, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x51, x + 0x08, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x60, x + 0x00, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x61, x + 0x08, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x70, x + 0x00, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x71, x + 0x08, y - 0x00, p, CHR_ALTPALETTE);
        break;
    case 0x3b: /* dancing snowman */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = 0x03;
        write_chr(img, chr_raw, 0x24, x + 0x08, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x25, x + 0x00, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x34, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x35, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x44, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x45, x + 0x00, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
    case 0x3c: /* abominable snowman */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette2;
        write_chr(img, chr_raw, 0x59, x + 0x08, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x5a, x + 0x00, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x69, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x6a, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x79, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7a, x + 0x00, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
    case 0x43: /* angry fish */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = 0x06;
        write_chr(img, chr_raw, 0x2e, x + 0x00, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x2f, x + 0x08, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x3e, x + 0x00, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x3f, x + 0x08, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x4e, x + 0x00, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x4f, x + 0x08, y - 0x00, p, CHR_ALTPALETTE);
        break;
    case 0x44: /* hermit crab */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette1;
        write_chr(img, chr_raw, 0x00, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x01, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x10, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x11, x + 0x00, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
    case 0x46: /* drill enemy */
        chr_raw = enemy_groups[/*level->enemy_group*/0xe].chr_raw;
        p = 0x02;
        write_chr(img, chr_raw, 0x04, x + 0x04, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x08, x + 0x00, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x09, x + 0x08, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x18, x + 0x00, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x19, x + 0x08, y - 0x00, p, CHR_ALTPALETTE);
        break;
#if 0
    case 0x48: /* helicopter enemy */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette1;
        write_chr(img, chr_raw, 0x0e, x + 0x08, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0f, x + 0x00, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1e, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1f, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x2e, x + 0x08, y + 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x2f, x + 0x00, y + 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
    case 0x49: /* snail */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette1;
        write_chr(img, chr_raw, 0x30, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x31, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x40, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x41, x + 0x00, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
    case 0x4a: /* left facing ghost shooter thing */
    case 0x4b: /* right facing ghost shooter thing */
        /* don't draw, launcher is built into background */
        break;
    case 0x4c: /* little rock dude */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette1;
        write_chr(img, chr_raw, 0x08, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x09, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x18, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x19, x + 0x00, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
    case 0x4f: /* spring enemy */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette2;
        write_chr(img, chr_raw, 0x0d, x + 0x08, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0c, x + 0x00, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1d, x + 0x08, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1c, x + 0x00, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x2d, x + 0x08, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x2c, x + 0x00, y - 0x00, p, CHR_ALTPALETTE);
        break;
    case 0x54: /* floating spring */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette2;
        write_chr(img, chr_raw, 0x24, x + 0x00, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x25, x + 0x08, y - 0x00, p, CHR_ALTPALETTE);
        break;
    case 0x59: /* magic bag boss */
        /* the level actually loads a new enemy group halfway
         * through the stage, so just hardcode it here */
        chr_raw = enemy_groups[0x12].chr_raw;
        p = 0x03;
        write_chr(img, chr_raw, 0x5d, x + 0x00, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x5e, x + 0x08, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x5f, x + 0x10, y - 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x6d, x + 0x00, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x6e, x + 0x08, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x6f, x + 0x10, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7d, x + 0x00, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7e, x + 0x08, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7f, x + 0x10, y - 0x00, p, CHR_ALTPALETTE);
        break;
    case 0x5a: /* moon tongue */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette1;
        write_chr(img, chr_raw, 0x3c, x + 0x04, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x4c, x + 0x04, y - 0x00, p, CHR_ALTPALETTE);
        break;
    case 0x5c: /* little rock dude on moon */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette1;
        write_chr(img, chr_raw, 0x00, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x01, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x10, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x11, x + 0x00, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
    case 0x5d: /* caveman */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette1;
        write_chr(img, chr_raw, 0x08, x + 0x08, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x09, x + 0x00, y - 0x10, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x18, x + 0x08, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x19, x + 0x00, y - 0x08, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x28, x + 0x08, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x29, x + 0x00, y - 0x00, p, CHR_MIRROR | CHR_ALTPALETTE);
        break;
#endif
    case 0x50: /* zipper (new level) */
    case 0x51: /* zipper (within level) */
    case 0x52: /* all below are zippers from puzzleworld 3 */
    case 0x80:
    case 0x81:
    case 0x82:
    case 0x83:
    case 0x84:
    case 0x85:
    case 0x86:
    case 0x87:
    case 0x88:
    case 0x89:
    case 0x8a:
    case 0x8b:
    case 0x8c:
    case 0x8d:
    case 0x8e:
    case 0x8f:
    case 0x90:
    case 0x91:
    case 0x92:
    case 0x93:
    case 0x94:
    case 0x95:
    case 0x96:
    case 0x97:
        chr_raw = std_actor;
        write_chr(img, chr_raw, 0x37, x + 0x04, y - 0x20, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x47, x + 0x04, y - 0x18, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x57, x + 0x04, y - 0x10, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x67, x + 0x04, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x77, x + 0x04, y + 0x00, std_palette, CHR_ALTPALETTE);
        break;
    case 0x60: /* pickup block */
        chr_raw = std_actor;
        write_chr(img, chr_raw, 0x4c, x + 0x00, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x4d, x + 0x08, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x5c, x + 0x00, y - 0x00, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x5d, x + 0x08, y - 0x00, std_palette, CHR_ALTPALETTE);
        break;
    case 0x61: /* pickup block slot */
        chr_raw = std_actor;
        write_chr(img, chr_raw, 0x4e, x + 0x00, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x4f, x + 0x08, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x5e, x + 0x00, y - 0x00, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x5f, x + 0x08, y - 0x00, std_palette, CHR_ALTPALETTE);
        break;
    case 0x70: /* cloud platform (slow) */
    case 0x71: /* cloud platform (med) */
    case 0x72: /* cloud platform (fast) */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = 0x03;
        write_chr(img, chr_raw, 0x6d, x - 0x04, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x6e, x + 0x04, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x6f, x + 0x0c, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7d, x - 0x04, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7e, x + 0x04, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7f, x + 0x0c, y - 0x00, p, CHR_ALTPALETTE);
        break;
#if 0
    case 0x73: /* conveyor belt platform */ /* not implemented in proto */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette2;
        write_chr(img, chr_raw, 0x0c, x + 0x04, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0d, x + 0x0c, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x0e, x + 0x14, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1c, x + 0x04, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1d, x + 0x0c, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x1e, x + 0x14, y - 0x00, p, CHR_ALTPALETTE);
        break;
#endif
    case 0x74: /* boat */
        chr_raw = std_actor;
        write_chr(img, chr_raw, 0x28, x + 0x00, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x29, x + 0x08, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x38, x + 0x00, y - 0x00, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x39, x + 0x08, y - 0x00, std_palette, CHR_ALTPALETTE);
        break;
    case 0x76: /* floating platform */
    case 0x77: /* falling platform */
        chr_raw = std_actor;
        write_chr(img, chr_raw, 0x68, x - 0x04, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x69, x + 0x04, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x6a, x + 0x0c, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x78, x - 0x04, y - 0x00, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x79, x + 0x04, y - 0x00, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7a, x + 0x0c, y - 0x00, std_palette, CHR_ALTPALETTE);
        break;
    case 0x78: /* ronald platform */
        chr_raw = enemy_groups[level->enemy_group].chr_raw;
        p = level->enemy_palette2;

        write_chr(img, chr_raw, 0x08, x + 0x00, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x09, x + 0x08, y - 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x18, x + 0x00, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x19, x + 0x08, y - 0x00, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x28, x + 0x00, y + 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x29, x + 0x08, y + 0x08, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x38, x + 0x00, y + 0x10, p, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x39, x + 0x08, y + 0x10, p, CHR_ALTPALETTE);
        break;
    case 0x79: /* super spring */
    case 0x7a: /* super spring (upside down) */
        chr_raw = std_actor;
        write_chr(img, chr_raw, 0x68, x + 0x00, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x69, x + 0x08, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x69, x + 0x10, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x6a, x + 0x18, y - 0x08, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x78, x + 0x00, y - 0x00, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x79, x + 0x08, y - 0x00, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x79, x + 0x10, y - 0x00, std_palette, CHR_ALTPALETTE);
        write_chr(img, chr_raw, 0x7a, x + 0x18, y - 0x00, std_palette, CHR_ALTPALETTE);
        break;
#endif
    default:
        printf("WARNING: Unknown actor type 0x%02x\n", (unsigned int)level->actors.type[idx]);
        write_overlay(img, level->actors.x[idx], level->actors.y[idx], IMG_ACTOR);
        break;
    }
}

/* eh, it didn't look good */
static void draw_kid(FIBITMAP *img, unsigned char tx, unsigned char ty)
{
    unsigned int x = tx * 16;
    unsigned int y = ty * 16 - 8;
    write_chr(img, std_actor, 0x02, x + 0x00, y - 0x18, 0x01, CHR_ALTPALETTE);
    write_chr(img, std_actor, 0x03, x + 0x08, y - 0x18, 0x01, CHR_ALTPALETTE);
    write_chr(img, std_actor, 0x12, x + 0x00, y - 0x10, 0x01, CHR_ALTPALETTE);
    write_chr(img, std_actor, 0x13, x + 0x08, y - 0x10, 0x01, CHR_ALTPALETTE);
    write_chr(img, std_actor, 0x0c, x + 0x00, y - 0x08, 0x01, CHR_ALTPALETTE);
    write_chr(img, std_actor, 0x0d, x + 0x08, y - 0x08, 0x01, CHR_ALTPALETTE);
    write_chr(img, std_actor, 0x18, x + 0x00, y + 0x00, 0x01, CHR_ALTPALETTE);
    write_chr(img, std_actor, 0x19, x + 0x08, y + 0x00, 0x01, CHR_ALTPALETTE);
}

static void output_image(unsigned int level_offs, struct level *level)
{
    size_t y, x;
    int i;
    char filename[256];
    FIBITMAP *img;
    const char *level_name = NULL;
    int has_kid_spot = 0;

    for(i = 0; i < sizeof(level_names) / sizeof(*level_names); ++i){
        if(level_names[i].offs == level_offs){
            level_name = level_names[i].name;
            has_kid_spot = level_names[i].has_kid_spot;
            break;
        }
    }

    strcpy(filename, rom_file);
    strcat(filename, "_dump");
    mkdir(filename, 0755);

    if(level_name)
        sprintf(filename, "%s_dump/%s.png", rom_file, level_name);
    else
        sprintf(filename, "%s_dump/level_%05x.png", rom_file, level_offs);
    printf("filename: %s\n", filename);

    bg = &colors[level->bg];

    img = FreeImage_Allocate(level->width * 16, level->height * 16, 24,
            FI_RGBA_RED_MASK, FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK);

    FreeImage_FillBackground(img, bg, 0);

    for(y = 0; y < level->height; ++y){
        for(x = 0; x < level->width; ++x){
            write_tile_image(img, level, x, y);
        }
    }

    if(has_kid_spot)
        draw_kid(img, level->start_x, level->start_y);

    for(i = 0; i < level->num_actors; ++i){
        write_actor(img, level, i);
    }

    FreeImage_FlipVertical(img);
    FreeImage_Save(FIF_PNG, img, filename, PNG_DEFAULT);

    FreeImage_Unload(img);
}

static void output_chr_image(unsigned int chr_offs, unsigned char *chr_raw)
{
    size_t y, x;
    int i;
    char filename[256];
    FIBITMAP *img;

    strcpy(filename, rom_file);
    strcat(filename, "_dump");
    mkdir(filename, 0755);

    sprintf(filename, "%s_dump/chr_%05x.png", rom_file, chr_offs);

    img = FreeImage_Allocate(16 * 8, 8 * 8, 24,
            FI_RGBA_RED_MASK, FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK);

    FreeImage_FillBackground(img, &colors[0x22], 0);

    i = 0;
    for(y = 0; y < 8; ++y)
        for(x = 0; x < 16; ++x){
            write_chr(img, chr_raw, i, x * 8, y * 8, 0xb, 0);
            ++i;
        }

    FreeImage_FlipVertical(img);
    FreeImage_Save(FIF_PNG, img, filename, PNG_DEFAULT);

    FreeImage_Unload(img);
}

static int read_palettes()
{
    unsigned int i;
#ifndef PROTO
    size_t offs = 0x2000;
#else
    size_t offs = 0x137e;
#endif

    if(lseek(fd, offs + 0x10 /* ines */, SEEK_SET) == (off_t)-1){
        perror("lseek");
        return 1;
    }

    if(read(fd, &palettes, sizeof(palettes)) < 0){
        perror("read");
        return 1;
    }

    for(i = 0; i < 0x20; ++i){
        printf("palettes[0x%02x]: 0x%02x 0x%02x 0x%02x\n",
                i, palettes.c1[i], palettes.c2[i], palettes.c3[i]);
    }

    if(read(fd, &alt_palettes, sizeof(alt_palettes)) < 0){
        perror("read");
        return 1;
    }

#ifndef PROTO
    for(i = 0; i < 0xf; ++i){
#else
    for(i = 0; i < 0xa; ++i){
#endif
        printf("alt_palettes[0x%02x]: 0x%02x 0x%02x 0x%02x\n",
                i, alt_palettes.c1[i], alt_palettes.c2[i], alt_palettes.c3[i]);
    }

    return 0;
}

static int read_enemy_groups()
{
    unsigned int i;
#ifndef PROTO
    size_t offs = 0xf31;
#else
    size_t offs = 0x570;
#endif
    unsigned char gfx_banks[0x14];

    if(lseek(fd, offs + 0x10 /* ines */, SEEK_SET) == (off_t)-1){
        perror("lseek");
        return 1;
    }

    if(read(fd, gfx_banks, sizeof(gfx_banks)) < 0){
        perror("read");
        return 1;
    }

    for(i = 0; i < 0x14; ++i){
        unsigned int offs = 0x20000 + 0x400 * gfx_banks[i] + 0x10 /* iNES */, j, x, y;
        struct enemy_group *group = &enemy_groups[i];
        lseek(fd, offs, SEEK_SET);
        read(fd, group->chr_raw, sizeof(group->chr_raw));

        printf("enemy group 0x%x (bank 0x%x):\n", i, gfx_banks[i]);
        printf("chr bank 0x%x (0x%x):\n", gfx_banks[i], offs);
        for(j = 0; j < 0x800 / 16; ++j){
            unsigned char *raw = &group->chr_raw[j * 16];
            for(y = 0; y < 8; ++y){
                for(x = 0; x < 8; ++x){
                    unsigned char color = ((raw[y] >> (7 - x)) & 1) | (((raw[y + 8] >> (7 - x)) & 1) << 1);
                    printf("%u", (unsigned int)color);
                }
                printf("\n");
            }
            printf("\n");
        }

        output_chr_image(offs, group->chr_raw);
    }

    printf("standard actor set:\n");
#ifndef PROTO
    printf("chr bank 0x2 (0x%x):\n", 0x20000 + 0x400 * 2 + 0x10);
    lseek(fd, 0x20000 + 0x400 * 2 + 0x10 /* iNES */, SEEK_SET);
#else
    printf("chr bank 0x0 (0x%x):\n", 0x20000 + 0x10);
    lseek(fd, 0x20000 + 0x10 /* iNES */, SEEK_SET);
#endif
    read(fd, std_actor, sizeof(std_actor));

    return 0;
}

static int read_byte()
{
    unsigned char byte;

    if(read(fd, &byte, 1) < 0){
        perror("read");
        exit(1);
    }

    bits <<= 8;
    bits |= byte;
    valid += 8;
//    printf("read: %02x\n", (unsigned int)byte);

    return 0;
}

static unsigned int get_mask(unsigned int width)
{
    unsigned int ret = 0, f = 1;

    while(width){
        ret |= f;
        f <<= 1;
        --width;
    }

    return ret;
}

static void uncompress(unsigned char *out, size_t out_len, unsigned int seek_bits,
        unsigned int copy_bits)
{
#ifndef PROTO
    size_t offs = 0;
    unsigned int seek_mask = get_mask(seek_bits), copy_mask = get_mask(copy_bits);
    unsigned int seek, copy, width = seek_bits + copy_bits + 1;

    valid = 0;
    bits = 0;

    while(offs < out_len){
        if(valid == 0)
            read_byte();

        if(bits & 1 << (valid - 1)){
            //uncompressed
            //D = Data
            //1DDDDDDDD
            while(valid < 9)
                read_byte();

            out[offs] = (bits >> (valid - 9)) & 0xFF;
//            printf("storing uncompressed %02x at 0x%x\n", out[offs], offs);

            ++offs;
            valid -= 9;
        }else{
            //compressed
            //S = Seek
            //C = Copy
            //0SSSSSSSSSSCCCCCC
            //the width of the seek and copy portions of the compressed
            //information is programmable per-level.  see copy_bits and
            //seek_bits, above.
            while(valid < width)
                read_byte();

            copy = ((bits >> (valid - width)) & copy_mask) + 3;
            seek = (bits >> (valid - width + copy_bits)) & seek_mask;
            if(!seek){
//                printf("end of data\n");
                break;
            }
/*            printf("copying 0x%02x bytes from 0x%02x bytes ago (%x): ",
                    (unsigned int)copy, (unsigned int)seek, offs - seek);*/

            while(copy){
                out[offs] = out[offs - seek];
//                printf("%02x ", (unsigned int)out[offs]);
                ++offs;
                --copy;
            }
//            printf("\n");

            valid -= width;
        }
    }

#else

    /* prototype compression:
     * every 8 bytes, there is a "compression" byte, which is a bitmask of
     * which following bytes are uncompressed and which are 2-byte
     * seek-and-copy commands.
     *
     * Examples:
     * 0xFF == 0b11111111 == next 8 bytes should be copied directly
     *
     * 0x77 == 0b01110111 == first 3 bytes are direct, then read the next two bytes as a seek-and-copy command, then next 3 are direct etc.
     *
     * seek-and-copy command works like:
     * 1st byte: xxxxxxxx
     * 2nd byte: yyyyzzzz
     * seek: yyyyxxxxxxxx bytes back and copy zzzz+3 bytes
     */
    size_t offs = 0;
    int is_compressed;
    unsigned char mask, byte1, byte2;
    unsigned int seek, copy, mask_bits;

    valid = 0;
    mask_bits = 0;

    while(offs < out_len){
        if(mask_bits == 0){
            bits = 0;
            read_byte();
            mask = bits;
            mask_bits = 8;
//            printf("new mask: %02x\n", (unsigned int)mask);
        }

        is_compressed = !(mask & 0x1);
        mask >>= 1;
        --mask_bits;

        if(is_compressed){
            bits = 0;
            read_byte();
            byte1 = bits;

            bits = 0;
            read_byte();
            byte2 = bits;

            seek = byte1 | (((unsigned int)(byte2 & 0xF0)) << 4);
            copy = (byte2 & 0xF) + 3;
            if(seek == 0 && copy == 3)
                /* end of data */
                break;

//            printf("copying %02x bytes from %02x bytes ago:", copy, seek);

            while(copy){
                out[offs] = out[offs - seek];
//                printf("%02x ", (unsigned int)out[offs]);
                ++offs;
                --copy;
            }

//            printf("\n");
        }else{
            bits = 0;
            read_byte();
            out[offs] = bits;
//            printf("storing uncompressed %02x at 0x%x\n", out[offs], offs);
            ++offs;
        }
    }
#endif
}

static void read_tileset(unsigned int offs, struct tileset *set)
{
    unsigned char *data = set->tile_data;
    unsigned char compression;
    unsigned int seek_bits, copy_bits, data_offs = 0, i, ntiles;

    printf("Tileset data starting at: 0x%x\n", offs);

    if(lseek(fd, offs, SEEK_SET) == (off_t)-1){
        perror("lseek");
        return;
    }

#ifndef PROTO
    read(fd, &compression, 1);
    seek_bits = compression >> 4;
    copy_bits = compression & 0xF;
#endif

    uncompress(set->tile_data, sizeof(set->tile_data), seek_bits, copy_bits);

    /* data[0] => ??? compared to some values from 0x069A-D (0xFE x 4), fails all comparisons on ronald-map (0x00), stored into 0x069d
     * data[1] => ??? compared to same values (now: 0xFE x 3, 0x00), ronald-map fails all, stored at 0x069c
     * data[2] => ??? compared again (now: 0xFE, 0xFE, 0x01, 0x00), again fails all, stored at 0x069b
     * data[3] => ??? compared again (now: 0xFE, 0x02, 0x01, 0x00), again fails all, stored at 0x069a
     * data[4] => ntiles
     * 5 x ntiles bytes of data used, then:
     * loads new ntiles from the next byte of data, then:
     * 2 x ntiles */

    set->palettes[0] = data[0];
    set->palettes[1] = data[1];
    set->palettes[2] = data[2];
    set->palettes[3] = data[3];
    printf("palettes: 0x%02x 0x%02x 0x%02x 0x%02x\n",
            (unsigned int)set->palettes[0],
            (unsigned int)set->palettes[1],
            (unsigned int)set->palettes[2],
            (unsigned int)set->palettes[3]);
    set->width1 = ntiles = data[4];
    printf("ntiles: 0x%02x\n", ntiles);

    data_offs = 5;

    set->tile_table = &set->tile_data[data_offs];
    printf("top left:     ");
    for(i = 0; i < ntiles; ++i, ++data_offs)
        printf("%02x ", (unsigned int)data[data_offs]);
    printf("\n");

    printf("top right:    ");
    for(i = 0; i < ntiles; ++i, ++data_offs)
        printf("%02x ", (unsigned int)data[data_offs]);
    printf("\n");

    printf("bottom right:  ");
    for(i = 0; i < ntiles; ++i, ++data_offs)
        printf("%02x ", (unsigned int)data[data_offs]);
    printf("\n");

    printf("bottom left: ");
    for(i = 0; i < ntiles; ++i, ++data_offs)
        printf("%02x ", (unsigned int)data[data_offs]);
    printf("\n");

    printf("color?:       ");
    for(i = 0; i < ntiles; ++i, ++data_offs)
        printf("%02x ", (unsigned int)data[data_offs]);
    printf("\n");

    set->width2 = ntiles = data[data_offs];
    ++data_offs;
    printf("ntiles: 0x%02x\n", ntiles);

    printf("???: ");
    for(i = 0; i < ntiles; ++i, ++data_offs)
        printf("%02x ", (unsigned int)data[data_offs]);
    printf("\n");

    printf("???: ");
    for(i = 0; i < ntiles; ++i, ++data_offs)
        printf("%02x ", (unsigned int)data[data_offs]);
    printf("\n");

    printf("\n");
}

static void build_tile_table(struct tileset *set)
{
    unsigned int offs = 0x20000 + 0x400 * set->gfx_bank + 0x10 /* iNES */, i, x, y;

    printf("reading CHR data starting at 0x%x\n", offs);
    if(lseek(fd, offs, SEEK_SET) == (off_t)-1){
        perror("lseek");
        return;
    }

    memset(set->chr_raw, 0, sizeof(set->chr_raw));
    read(fd, set->chr_raw, sizeof(set->chr_raw));

    for(i = 0; i < 0x400 / 16; ++i){
        unsigned char *raw = &set->chr_raw[i * 16];
        for(y = 0; y < 8; ++y){
            for(x = 0; x < 8; ++x){
                unsigned char color = ((raw[y] >> (7 - x)) & 1) | (((raw[y + 8] >> (7 - x)) & 1) << 1);
                printf("%u", (unsigned int)color);
            }
            printf("\n");
        }
        printf("\n");
    }

    output_chr_image(offs, set->chr_raw);
}

static int read_tilesets(unsigned int table_offs)
{
    unsigned int i;
#ifndef PROTO
    const unsigned int num_tilesets = 0x2c;
    unsigned char ts_data[4][0x2c];
#else
    const unsigned int num_tilesets = 0x28;
    unsigned char ts_data[4][0x28];
#endif

    if(lseek(fd, table_offs, SEEK_SET) == (off_t)-1){
        perror("lseek");
        return 1;
    }

    if(read(fd, ts_data, sizeof(ts_data)) < 0){
        perror("read");
        return 1;
    }

    for(i = 0; i < num_tilesets; ++i){
        struct bank *bank = &banks[ts_data[2][i]];
        unsigned int addr = (ts_data[1][i] << 8) | ts_data[0][i];
        unsigned int set_offs = bank->offs + addr - bank->seek;
        printf("tileset 0x%x: gfx bank: 0x%x data bank: 0x%x, addr: 0x%x, offset: 0x%x (+0x10 ines)\n",
                i, ts_data[3][i], ts_data[2][i], addr, set_offs);

        tilesets[i].gfx_bank = ts_data[3][i];
        read_tileset(set_offs + 0x10, &tilesets[i]);
        build_tile_table(&tilesets[i]);
    }

    return 0;
}

static void read_level(unsigned int offs, struct level *level)
{
    int i = 0;
    unsigned int seek_bits = 0, copy_bits = 0;
    unsigned char compression;
    int hacked = 0;

    printf("Level data starting at: 0x%x\n", offs);

    if(lseek(fd, offs, SEEK_SET) == (off_t)-1){
        perror("lseek");
        return;
    }

    read(fd, &level->width, 1);
    read(fd, &level->height, 1);
    read(fd, &level->ts1, 1);
    read(fd, &level->ts2, 1);
    read(fd, &level->ts3, 1);
    read(fd, &level->ts4, 1);
#ifndef PROTO
    read(fd, &compression, 1);
#endif

    memset(level->data, 0, sizeof(level->data));

    printf("Map size: %u x %u\n", (unsigned int)level->width, (unsigned int)level->height);
    printf("ts1: 0x%02x\n", (unsigned int)level->ts1);
    printf("ts2: 0x%02x\n", (unsigned int)level->ts2);
    printf("ts3: 0x%02x\n", (unsigned int)level->ts3);
    printf("ts4: 0x%02x\n", (unsigned int)level->ts4);
#ifndef PROTO
    printf("compression: 0x%02x\n", (unsigned int)compression);
    seek_bits = compression >> 4;
    copy_bits = compression & 0xF;
#endif

#ifdef PROTO
    //hacks to fix incorrect tileset references in the game
    if(offs == 0xcf4e ||
            offs == 0xd415 ||
            offs == 0x14010){
        level->ts2 = 0x01;
        hacked = 1;
    }else if(offs == 0xbce9 ||
            offs == 0x12010){
        level->ts3 = 0x01;
        hacked = 1;
    }else if(offs == 0xe010){
        level->ts4 = 0x01;
        hacked = 1;
    }
    
    if(offs == 0x14e1d){
        level->enemy_group = 0x09;
        hacked = 1;
    }else if(offs == 0x13a92){
        level->enemy_group = 0x0b;
        hacked = 1;
    }else if(offs == 0x12010 ||
            offs == 0xe010){
        level->enemy_group = 0x0d;
        hacked = 1;
    }else if(offs == 0xb19a){
        level->enemy_group = 0x0e;
        hacked = 1;
    }

    /* bc54 -- i think the tileset order changed */
    /* fc88 -- ditto */
    if(hacked){
        printf("hacked!\n");
        printf("ts1: 0x%02x\n", (unsigned int)level->ts1);
        printf("ts2: 0x%02x\n", (unsigned int)level->ts2);
        printf("ts3: 0x%02x\n", (unsigned int)level->ts3);
        printf("ts4: 0x%02x\n", (unsigned int)level->ts4);
        printf("enemy_group: 0x%02x\n", (unsigned int)level->enemy_group);
    }
#endif

    uncompress(level->data, sizeof(level->data), seek_bits, copy_bits);

    level->num_actors = level->data[0];
    level->actors.x = level->data + 1;
    level->actors.y = level->data + 1 + level->num_actors;
    level->actors.type = level->data + 1 + level->num_actors * 2;
    level->actors.order = level->data + 1 + level->num_actors * 3;
    level->map = level->data + 1 + level->num_actors * 4;

    for(i = 0; i < level->num_actors; ++i){
        printf("Actor[0x%x]: pos=(0x%02x, 0x%02x) type=0x%02x order=0x%02x\n", i,
                (unsigned int)level->actors.x[i],
                (unsigned int)level->actors.y[i],
                (unsigned int)level->actors.type[i],
                (unsigned int)level->actors.order[i]);
    }
    for(i = 0; i < level->width * level->height; ++i){
        if(i % level->width == 0)
            printf("\n");
        printf("%02x ", (unsigned int)level->map[i]);
    }
    printf("\n");

    output_image(offs, level);
}

int main(int argc, char **argv)
{
    unsigned int offs, ts_offs, bytes, i;
    unsigned char data;
#ifndef PROTO
    const unsigned int num_maps = 0x5d;
#else
    const unsigned int num_maps = 0x4f;
#endif

    FreeImage_Initialise(FALSE);

#ifndef PROTO
    /* level table offset */
    offs = 0x214d;
    /* tileset table offset */
    ts_offs = 0x209d;
#else
    offs = 0x14ac;
    ts_offs = 0x140c;
#endif

    rom_file = basename(argv[1]);
    fd = open(argv[1], O_RDONLY);
    if(fd < 0){
        perror("open");
        return 1;
    }

    if(read_palettes())
        return 1;

    if(read_tilesets(ts_offs))
        return 1;

    if(read_enemy_groups())
        return 1;

    if(lseek(fd, offs, SEEK_SET) == (off_t)-1){
        perror("lseek");
        return 1;
    }

    for(bytes = 0; bytes < num_maps * sizeof(levels[0]); ++bytes){
        read(fd, &data, 1);
        *(((char *)&levels[bytes % num_maps]) + (bytes / num_maps)) = data;
    }

    for(i = 0; i < num_maps; ++i){
        struct bank *bank = &banks[levels[i].addr_bank];
        unsigned int addr = levels[i].addr_low | (levels[i].addr_high << 8);
        unsigned int level_offs = bank->offs + addr - bank->seek;
        printf("\n\nlevel[%u]:\n", i);
        printf("\t[0x%04x] start_x: 0x%02x\n", offs + i, (unsigned int)levels[i].start_x);
        printf("\t[0x%04x] start_y: 0x%02x\n", offs + i + num_maps * 1, (unsigned int)levels[i].start_y);
        printf("\t[0x%04x] bg: 0x%02x\n", offs + i + num_maps * 2,  (unsigned int)levels[i].bg);
        printf("\t[0x%04x] flags: 0x%02x\n", offs + i + num_maps * 3,  (unsigned int)levels[i].flags);
        printf("\t[0x%04x] enemy_group: 0x%02x\n", offs + i + num_maps * 4,  (unsigned int)levels[i].enemy_group);
        printf("\t[0x%04x] enemy_palette1: 0x%02x\n", offs + i + num_maps * 5,  (unsigned int)levels[i].enemy_palette1);
        printf("\t[0x%04x] enemy_palette2: 0x%02x\n", offs + i + num_maps * 6,  (unsigned int)levels[i].enemy_palette2);
        printf("\t[0x%04x] junk6: 0x%02x\n", offs + i + num_maps * 7,  (unsigned int)levels[i].junk6);
        printf("\t[0x%04x] card1: 0x%02x\n", offs + i + num_maps * 8,  (unsigned int)levels[i].card1);
        printf("\t[0x%04x] card2: 0x%02x\n", offs + i + num_maps * 9,  (unsigned int)levels[i].card2);
#ifndef PROTO
        printf("\t[0x%04x] music: 0x%02x\n", offs + i + num_maps * 10, (unsigned int)levels[i].music);
        printf("\t[0x%04x] addr_low: 0x%02x\n", offs + i + num_maps * 11, (unsigned int)levels[i].addr_low);
        printf("\t[0x%04x] addr_high: 0x%02x\n", offs + i + num_maps * 12, (unsigned int)levels[i].addr_high);
        printf("\t[0x%04x] addr_bank: 0x%02x\n", offs + i + num_maps * 13, (unsigned int)levels[i].addr_bank);
#else
        printf("\t[0x%04x] addr_low: 0x%02x\n", offs + i + num_maps * 10, (unsigned int)levels[i].addr_low);
        printf("\t[0x%04x] addr_high: 0x%02x\n", offs + i + num_maps * 11, (unsigned int)levels[i].addr_high);
        printf("\t[0x%04x] addr_bank: 0x%02x\n", offs + i + num_maps * 12, (unsigned int)levels[i].addr_bank);
#endif
        printf("\t\n");
        printf("\tlevel_offs: 0x%x ( + 0x10 INES)\n", level_offs);
        if(!bank->offs || addr < bank->seek)
            printf("WARNING: Skipping due to nonsense offset %x\n", addr);
        else
            read_level(level_offs + 0x10, &levels[i]);
    }

    close(fd);

    FreeImage_DeInitialise();

    return 0;
}
